## What is it?

This is a demo repository that demonstrates how to use Sass and TypeScript with Views and Cards in
the [Advanced Views plugin](https://wordpress.org/plugins/acf-views/).

You can copy the `tools` directory to your theme and use in your project.

### 1. How it works

The Advanced Views plugin offers the [File system storage](https://docs.acfviews.com/templates/file-system-storage)
option. When enabled, View and Card templates and assets
are stored in the 'advanced-views' folder within the current theme.

This repository illustrates how to use TypeScript instead of pure JavaScript and Sass instead of pure CSS. (We're
targeted on the `style.css` and `script.js` files of Views and Cards.)

If you need something one, TypeScript or Sass only, you can easily remove the unnecessary package from
the `tools/package.json` by `yarn remove {x}`.

### 2. How to use it

1. Copy the `bundlers` directory to your theme and enter to this folder.
2. Run `corepack use yarn@latest` (since v2, yarn is integrated into nodejs,
   and [can be activated](https://yarnpkg.com/getting-started/install) on any folder). 
3. Run `yarn install`
4. Use `yarn watch/build` commands

### 3. Implementation details

This repository utilizes plain [Sass](https://www.npmjs.com/package/sass)
and [TypeScript](https://www.npmjs.com/package/typescript) packages to compile Sass and TypeScript. Both packages offer
watch options. If you prefer additional wrappers like [Webpack](https://webpack.js.org/)
or [Laravel Mix](https://laravel-mix.com/), you're free to use them.

The current setup only compiles assets without minifying them. We do not recommend minifying assets for the following
reasons:

**a) Loss of compatibility with the Advanced Views on-site code editor**

Once assets are minified, it becomes impossible to edit them using the on-site code editor. This means that if you hand
off the project, the next developer won't be able to make even small changes without delving into the theme.

Additionally, you'll lose the ability to make quick amendments, which are sometimes necessary for urgent fixes on a live
website.

**b) Built-in minification in the Advanced Views plugin**

The Advanced Views plugin includes basic asset minification functionality. While it may not be as powerful as Webpack or
Laravel Mix, it's sufficient for most use cases.